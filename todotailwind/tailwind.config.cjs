/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
    screens: {
      // If i set a custom breakpoint, I need to also define my other custom breakpoints that I will use,
      // Cannot mix and match with default and custom
      'xs': '380px',
      // => @media (min-width: 380px) { ... }
      'md': '790px',
      // => @media (min-width: 600px)
    },
  },
  plugins: [],
}
