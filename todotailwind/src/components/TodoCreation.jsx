// This functional component will actually deal with all of the task creations and the task list creation.
// The actual todos from the todo list will be passed to the functional component Todos which will display it.
// Think of this file as the backend essentially.
import React, { useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import Todos from "./Todos";



export default function TodoCreation() {
    
    const [todoList, setTodoList] = useState([]);
    const [todoTask, SetTodoTask] = useState("");
    const id = uuidv4();

    // this function allows me to filter the todolist and return the items of the array that do NOT MATCH the id given to delete.
    function deleteTask(id) {
        setTodoList(todoList.filter((task) => task.id !== id));
    }

    // within this function, I should look for the tasks that match the given ID, and change a field of it.
    function editTask(id, content) {
        setTodoList(
            todoList.map( (task) => {
                if (task.id == id) {
                    return {...task, content: content}
                }
                else {
                    return task;
                }
            })
        );
    }
// the spread operator here means that JS keeps the rest of the object the same except the property to change which is completed. the else part returns the remaining tasks.
    function completeTask(id) {
        setTodoList(
            todoList.map( (task) => {
                if (task.id == id) {
                    return {...task, completed: true}       
                }
                else {
                    return task;
                }
            })
        );
    }

    function handleInput(event){
        SetTodoTask(event.target.value);
    };

    function addTask(){
        const date = new Date();
        const newTodo = {
            id: id,
            content: todoTask,
            completed: false,
            timestamp: date.toUTCString(),
        }
        setTodoList(newTodo.content !== "" ? [...todoList, newTodo] : todoList);
        SetTodoTask("");
    };

    const todoComponents = todoList.map(todo => {
        // I added a br tag between components so that after each todo is created, a space is placed.
        return (
            <div key={todo.id}>
                <Todos 
                // key={todo.id}
                id={todo.id}
                content={todo.content}
                completion={todo.completed}
                timestamp={todo.timestamp}
                deleteTask={deleteTask}
                editTask={editTask}
                completeTask={completeTask}
                />
                <br/>
            </div>
        );

    });

    return (
        <div className="bg-cyan-600 py-3 bg-opacity-90">
            <div className="display-flex flex-row justify-center py-4 content-center max-w-sm mx-auto gap-3">
                <label htmlFor="inputField" className="block text-gray-300 text-sm md:text-base font-thin md:font-bold p-1">Enter task: </label>
                <input type="text" placeholder="Enter your task" id="inputField" className="border rounded-lg py-1 px-1" onChange={handleInput} value={todoTask}/>
                <button id="addButton" className="border text-sm md:text-base text-gray-300 rounded-lg hover:rounded-lg hover:bg-slate-200 hover:text-cyan-700 p-1 hover:ring-2 hover:ring-green-800 transition ease-in" onClick={addTask}>Add Task</button>
            </div>
            <div>
                {todoComponents}
            </div>
        </div>
    );


}