import React from "react";
import Welcome from "./Welcome";
import TodoCreation from "./TodoCreation";


function App() {
    return (
        <div >
            <Welcome />
            <TodoCreation />
        </div>
    );

}

export default App;