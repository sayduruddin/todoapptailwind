// This file will only deal with the creation of the actual todos, and info will be passed in as props.
import React, { useState } from "react";

export default function Todos(props) {

    // this state variable allows me to check if the edit state is selected or not.
    // if selected, then user WANTS to edit so the save and cancel options should be shown.
    const [editState, setEditState] = useState(false);
    
    // This state variable should contain the input when a user tries to edit a todo.
    const [editContent, setEditContent] = useState("");

    // this is the event handler for the onChange attribute for the input field when editing.
    function handleEditInput(event) {
        setEditContent(event.target.value);
    }
    // the updateEdit function should only work with updating the state of the editState variable, this is bound to the edit button only
    function toggleEditButton() {
        setEditState(editState => !editState);
    };

    // this saveContent function should call the parent method that will update the content of the todo.
    function saveContentOnEdit() {
        props.editTask(props.id, editContent);
        setEditState(editState => !editState);
    };



    // if editstate is true, it means the user would like to edit todo, so show save and cancel.
    if (editState) {

        return (
            <div className="flex flex-row justify-between rounded-lg mx-auto w-full sm:w-1/2 md:w-1/2 bg-gray-200 bg-opacity-20 p-2 shadow-lg hover:border box-border ">
                <div className="inline-block gap-4 p-2">
                    <div className="font-extralight text-gray-100 italic text-xs sm:text-sm md:text-base">
                        <span className="border rounded-full p-2 m-1 bg-black bg-opacity-30 border-black border-opacity-10">{props.timestamp}</span>
                    </div>
                    <div className="p-2 mt-3 ">
                        <input className="p-2 rounded-md bg-cyan-100 ring-cyan-500/50" type="text" placeholder={props.content} onChange={handleEditInput}></input>
                    </div>
                </div>
                <div className="flex flex-col gap-2 py-2">
                    <input type="checkbox" checked={props.completion} onChange={ () => { props.completeTask(props.id) } }></input>
                    <div className="inline-block">  
                        <button className="btn-action mr-auto" onClick={toggleEditButton}>Cancel</button>
                        <button className="btn-action" onClick={saveContentOnEdit}>Save</button>
                    </div>
                    <button className="btn-action" onClick={ () => { props.deleteTask(props.id) }}>Delete</button>
                </div>
                
            </div>
        )
    }
    // Else if the edit state is set to false, user does not want to edit it.
    else if (!editState) {
        return (
            <div className="flex flex-row justify-between rounded-lg mx-auto w-full sm:w-1/2 md:w-1/2 bg-gray-200 bg-opacity-20 py-2 shadow-lg hover:border box-border">
                <div className="inline-block gap-4 p-2">
                    {/* I added relative to the parent so I can make child absolute and position timestamp */}
                    {/* Need to rework and figure out exactly how I want my stuff to be laid out, i also added absolute top-0 to position 0 from the top */}
                    <div className="font-extralight text-gray-100 italic text-xs sm:text-sm md:text-base">
                        <span className="border rounded-full p-2 m-1 bg-black bg-opacity-30 border-black border-opacity-10">{props.timestamp}</span>
                    </div>
                    <div className="p-2 mt-3">
                        <span className={ props.completion ? "line-through italic p-2" : "p-2"}>{props.content}</span>
                    </div>
                </div>
                <div className="flex flex-col gap-2 p-2">
                    <input type="checkbox" checked={props.completion} onChange={ () => { props.completeTask(props.id) } }></input>
                    <button className={ props.completion ? "btn-action cursor-not-allowed" : "btn-action" }  onClick={toggleEditButton} disabled={props.completion}>Edit</button>
                    <button className="btn-action" onClick={ () => {props.deleteTask(props.id)} }>Delete</button>
                </div>
            </div>
            /* 

            */
        )
    }


}

{/* <div className={props.completion ? 'line-through italic' : 'auto'}>
                        <span className="p-2">{props.content}</span>
                    </div> */}