import React from "react";

export default function Welcome() {
    const author = "Saydur Uddin";
    const date = new Date();
    const month = date.getMonth()+1

    return (
        // className is used with Tailwind CSS.
        // "bg-gradient-to-r from-green-400 to-blue-500  "
        <div className="py-6 max-w-full text-center bg-cyan-700 text-gray-200">
            <div className="flex flex-col flex-wrap justify-center content-center">
                <h1 className="text-4xl md:text-5xl py-1 font-semibold">To Do Application</h1>
                <h2 className="text-base md:text-lg py-1 font-thin">The author of this app is {author}</h2>
                <p className="text-xs md:text-base py-1 font-light">The current date is {date.getDate() + "/" + month + "/" + date.getFullYear()}</p>
            </div>
        </div>
    );
}